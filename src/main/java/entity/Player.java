package entity;

import game.GamePanel;
import game.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Player extends Entity {

    GamePanel gp;
    KeyHandler keyH;

    public final int screenX;
    public final int screenY;
    public int hasKey = 0;
    boolean cuttingTrees = false;
    boolean playerHaveAxe = false;

    public Player(GamePanel gp, KeyHandler keyH) {
        this.gp = gp;
        this.keyH = keyH;

        screenX = gp.screenWidth/2 - (gp.tileSize/2);
        screenY = gp.screenHeight/2 - (gp.tileSize/2);

        solidArea = new Rectangle();
        solidArea.x = 16;
        solidArea.y = 25;
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;
        solidArea.width = 18;
        solidArea.height = 24;

        setDefaultValues();
        getPlayerImage();
    }
    public void setDefaultValues() {
        worldX = gp.tileSize*23;
        worldY = gp.tileSize*21;
        speed = 8;
        direction = "down";
    }

    public void getPlayerImage() {
        try {

            up1 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_1.png" ) );
            up2 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_2.png" ) );
            up3 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_3.png" ) );
            up4 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_4.png" ) );
            up5 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_5.png" ) );
            up6 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_6.png" ) );
            up7 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_7.png" ) );
            up8 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_8.png" ) );
            up9 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_up_9.png" ) );
            down1 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_1.png" ) );
            down2 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_2.png" ) );
            down3 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_3.png" ) );
            down4 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_4.png" ) );
            down5 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_5.png" ) );
            down6 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_6.png" ) );
            down7 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_7.png" ) );
            down8 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_8.png" ) );
            down9 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_down_9.png" ) );
            left1 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_1.png" ) );
            left2 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_2.png" ) );
            left3 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_3.png" ) );
            left4 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_4.png" ) );
            left5 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_5.png" ) );
            left6 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_6.png" ) );
            left7 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_7.png" ) );
            left8 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_8.png" ) );
            left9 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_left_9.png" ) );
            right1 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_1.png" ) );
            right2 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_2.png" ) );
            right3 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_3.png" ) );
            right4 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_4.png" ) );
            right5 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_5.png" ) );
            right6 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_6.png" ) );
            right7 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_7.png" ) );
            right8 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_8.png" ) );
            right9 = ImageIO.read( getClass().getResourceAsStream( "/player/boy_right_9.png" ) );

        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update() {

        if (keyH.upPressed == true || keyH.downPressed == true ||
                keyH.leftPressed == true || keyH.rightPressed == true) {

            if (keyH.upPressed == true) {
                direction = "up";
            } else if (keyH.downPressed == true) {
                direction = "down";
            } else if (keyH.leftPressed == true) {
                direction = "left";
            } else if (keyH.rightPressed == true) {
                direction = "right";
            } else if (keyH.spacePressed == true) {
                cuttingTrees = true;
            }


            // CHECK TILE COLLISION
            collisionOn = false;
            gp.cChecker.checkTile( this );


            // CHECK OBJECT COLLISION
            int objIndex = gp.cChecker.checkObject( this, true );

            pickUpObjects( objIndex );
            cuttingTrees();

            //IF COLLISION IS FALSE, PLAYER CAN MOVE
            if (!collisionOn) {
                switch (direction) {
                    case "up":
                        worldY -= speed;
                        break;
                    case "down":
                        worldY += speed;
                        break;
                    case "left":
                        worldX -= speed;
                        break;
                    case "right":
                        worldX += speed;
                        break;
                }
            }

            spriteCounter++;
            if (spriteCounter > 12) {
                if (spriteNum == 1) {
                    spriteNum = 2;
                } else if (spriteNum == 2) {
                    spriteNum = 1;
                }
                spriteCounter = 0;
            }


//            spriteNum++;
//            if (spriteNum > 9) {
//                spriteNum = 1;
//            }else{
//                spriteNum = 1;
//            }
//
//            if (playerHaveAxe = true) {
//                if (cuttingTrees = true) {
//                    if (keyH.spacePressed)
//                        cuttingTrees = false;
//                }else {
//                    cuttingTrees = true;
//                }
//            }

//            if (spriteCounter > 8) {
//
//                if (spriteNum == 2) {
//                    spriteNum = 3;
//                }
//                else if(spriteNum == 3) {
//                    spriteNum = 4;
//                }
//                else if(spriteNum == 3) {
//                    spriteNum = 4;
//                }
//                else if(spriteNum == 4) {
//                    spriteNum = 5;
//                }
//                spriteCounter = 0;
            }else {
            spriteNum = 1;
        }

        }

        public void cuttingTrees() {

            if (playerHaveAxe = true) {
                if (cuttingTrees = true) {
                    if (keyH.spacePressed)
                        cuttingTrees = true;
                }else {
                    cuttingTrees = false;
                }
            }
        }

        public void pickUpObjects (int i) {

        if (i != 999) {

            String objectName = gp.obj[i].name;

            switch (objectName) {
                case "Key":
                    gp.playSE( 1 );
                    hasKey++;
                    gp.obj[i] = null;
                    gp.ui.showMessage( "You got a key!" );
                    break;
                case "Door":
                    if (hasKey > 0) {
                        gp.playSE( 3 );
                        gp.obj[i] = null;
                        hasKey--;
                        gp.ui.showMessage( "You opened a door!" );
                    }
                    else {
                        gp.ui.showMessage( "You need a key!" );
                    }
                    break;
                case "Boots":
                    gp.playSE( 2 );
                    speed += 4;
                    gp.obj[i] = null;
                    gp.ui.showMessage( "Speed up!" );
                    break;
                case "Chest":
                    gp.ui.gameFinished = true;
                    gp.stopMusic();
                    gp.playSE( 4 );
                    break;
                case "Axe":
                    gp.playSE( 2 );
                    playerHaveAxe = true;
                    cuttingTrees = true;
                    gp.obj[i] = null;
                    gp.ui.showMessage( "You can cut trees!" );
                    break;
                case "Hoe":
                    gp.playSE( 2 );
                    gp.obj[i] = null;
                    gp.ui.showMessage( "You can make a patch!" );
                    break;
            }
          }
        }

    public void draw(Graphics2D g2) {

//        g2.setColor( Color.BLUE );
//        g2.fillRect( x,y,gp.tileSize,gp.tileSize );

        BufferedImage image = null;

        switch (direction) {
            case "up":
                if (spriteNum == 1) {
                    image = up1;
                }
                if (spriteNum == 2) {
                    image = up2;
                }
                if (spriteNum == 3) {
                    image = up3;
                }
                if (spriteNum == 4) {
                    image = up4;
                }
                if (spriteNum == 5) {
                    image = up5;
                }
                if (spriteNum == 6) {
                    image = up6;
                }
                if (spriteNum == 7) {
                    image = up7;
                }
                if (spriteNum == 8) {
                    image = up8;
                }
                if (spriteNum == 9) {
                    image = up9;
                }
                break;
            case "down":
                if (spriteNum == 1) {
                    image = down1;
                }
                if (spriteNum == 2) {
                    image = down2;
                }
                if (spriteNum == 3) {
                    image = down3;
                }
                if (spriteNum == 4) {
                    image = down4;
                }
                if (spriteNum == 5) {
                    image = down5;
                }
                if (spriteNum == 6) {
                    image = down6;
                }
                if (spriteNum == 7) {
                    image = down7;
                }
                if (spriteNum == 8) {
                    image = down8;
                }
                if (spriteNum == 9) {
                    image = down9;
                }
                break;
            case "left":
                if (spriteNum == 1) {
                    image = left1;
                }
                if (spriteNum == 2) {
                    image = left2;
                }
                if (spriteNum == 3) {
                    image = left3;
                }
                if (spriteNum == 4) {
                    image = left4;
                }
                if (spriteNum == 5) {
                    image = left5;
                }
                if (spriteNum == 6) {
                    image = left6;
                }
                if (spriteNum == 7) {
                    image = left7;
                }
                if (spriteNum == 8) {
                    image = left8;
                }
                if (spriteNum == 9) {
                    image = left9;
                }
                break;
            case "right":
                if (spriteNum == 1) {
                    image = right1;
                }
                if (spriteNum == 2) {
                    image = right2;
                }
                if (spriteNum == 3) {
                    image = right3;
                }
                if (spriteNum == 4) {
                    image = right4;
                }
                if (spriteNum == 5) {
                    image = right5;
                }
                if (spriteNum == 6) {
                    image = right6;
                }
                if (spriteNum == 7) {
                    image = right7;
                }
                if (spriteNum == 8) {
                    image = right8;
                }
                if (spriteNum == 9) {
                    image = right9;
                }
                break;
        }
        g2.drawImage( image, screenX, screenY, gp.tileSize, gp.tileSize, null );
//        g2.setColor( Color.red );
//        g2.drawRect( screenX+solidArea.x, screenY+solidArea.y, solidArea.width, solidArea.height);
        //System.out.println(playerHaveAxe);

    }
}
