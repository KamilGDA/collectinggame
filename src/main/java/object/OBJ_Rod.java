package object;

import javax.imageio.ImageIO;
import java.io.IOException;

public class OBJ_Rod extends SuperObject{
    public OBJ_Rod() {
        name = "Rod";
        try {
            image = ImageIO.read( getClass().getResourceAsStream( "/objects/rod.png" ) );

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
