package object;

import javax.imageio.ImageIO;
import java.io.IOException;

public class OBJ_Scythe extends SuperObject {

    public OBJ_Scythe() {
        name = "Scythe";
        try {
            image = ImageIO.read( getClass().getResourceAsStream( "/objects/scythe.png" ) );

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
