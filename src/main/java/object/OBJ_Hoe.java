package object;

import javax.imageio.ImageIO;
import java.io.IOException;

public class OBJ_Hoe extends SuperObject {

    public OBJ_Hoe() {
        name = "Hoe";
        try {
            image = ImageIO.read( getClass().getResourceAsStream( "/objects/hoe.png" ) );

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
